#! /usr/bin/env python

# this program will take a cDNA and map it onto a genomic one. when you input you specify whether it is forward or reverse.
# if you use reverse then it takes the reverse complement so the ending output ranges are for the reverse complement of your gDNA.
# warning, this program will not count polymorphisms between the cDNa and genomic DNA, those will split exons.
usage = """

alignment.py requires 5 inputs:  1. gdna.fasta  2. cdna.fasta   3. kmer   4.trim_ends   5.forward/reverse

"""


import re
from Bio import SeqIO
from operator import itemgetter
from itertools import groupby
import sys


def seq_to_kmers(seq): # set up the kmers
	kmers = []
	for i in range(0,(len(seq)-kmer_length)):
		kmers.append(seq[i:i+kmer_length])
	return kmers



if (len(sys.argv) != 6): #if there are not the correct number of inputs then print the warning line
	print usage
else:# do the actual program
	trim_ends = int(sys.argv[4]) # have to specify that it is an integer
	kmer_length = int(sys.argv[3])
	gDNA_filename = sys.argv[1]
	cDNA_filename = sys.argv[2]
	print "Reading data..." #checkpoint to make sure it's working

	# Read in genome
	gDNA = "" # set gDNA as an empty string
	for record in SeqIO.parse(gDNA_filename, "fasta") : # parse out gDNA
		gDNA = str(record.seq).lower() #make the gDNA into all lowercase

	print "Genome length:", len(gDNA)


	# read in cDNA


	cDNA_list = []

	if sys.argv[5] == "forward": # if the user specifies forward go through this loop
		for record in SeqIO.parse(cDNA_filename, "fasta") : # parsel out the cDNA
			cDNA_list.append(str(record.seq)[trim_ends:-trim_ends].lower())# trims the ends of the sequences
	if sys.argv[5] == "reverse": # if the user specifies reverse then do the reverse complement
		for record in SeqIO.parse(cDNA_filename, "fasta"): # parsel out the cDNA
			cDNA_list.append(str(record.seq.reverse_complement())[trim_ends:-trim_ends].lower())#flips it to reverse complement and trims it



	print "Making cDNA kmers..."

	kmers = set()
	for cDNA in cDNA_list:
		kmers.update( seq_to_kmers(cDNA) )#split the cDNA into kmers

	print "Number of kmers:", len(kmers)

	print "Searching for kmers in gDNA..."

	starts = set()
	for kmer in kmers: # sets up the kmers which are the data broken into equal chunks positioned next to eachother.
		position = gDNA.find(kmer)# looks for kmers in the gDNA sequences
		hits = [m.start() for m in re.finditer('(?={})'.format(kmer), gDNA)]
		starts.update( hits )#records where the gDNA matches to the kmers



	mapped = [False] * len(gDNA) # makes all valuse in gDNA false

	for start in starts:
		for i in range(start, start+kmer_length):
			mapped[i] = True # Turns the position of any matches between cDNA and gDNA true in the gDNA



	mapped_sites = []
	out_sites = []
	for i in range(0,len(gDNA)):# takes the i values from the term before
		site = gDNA[i]# maps the i values to the gDNA file
		if mapped[i]:
			site = site.upper()#turns the mapped gDNA sites uppercase
			mapped_sites.append( i ) #checks for more than one instance
		out_sites.append( site )


	out_string = ''.join(out_sites)#joins together strings of adjascent kmers.

	print out_string

	ranges = []
	ex_count = 0
	for k, g in groupby(enumerate(mapped_sites), lambda (i,x):i-x):# takes the list of mapped sites and prints out their ranges
		group = map(itemgetter(1), g)
		ex_count = ex_count + 1
		ranges.append((group[0], group[-1]))
		print "exon %d: " % (ex_count), ranges[-1]


	print "there are %d exons" % len(ranges[0:]) #prints out how many ranges (exons) were found
	print "number of mapped sites:", len(mapped_sites)






